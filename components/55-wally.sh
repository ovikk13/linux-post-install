#!/usr/bin/env bash

set -e

home=/home/$INSTALL_USER

if command -v wally > /dev/null 2>&1; then
    if ./helpers/ask-user.sh 'Wally already appears to be installed. Skip?'; then
        echo 'Skipping...'
        exit
    fi
fi

# dependencies
apt-get install libusb-dev

# create udev file. from: https://github.com/zsa/wally/wiki/Linux-install
cat << EOF > /etc/udev/rules.d/50-wally.rules
# STM32 rules for the Planck EZ
SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", MODE:="0666", SYMLINK+="stm32_dfu"
EOF

# make sure user is part of plugdev
if ! grep -q plugdev /etc/group; then
    groupadd plugdev
fi
usermod -aG plugdev $INSTALL_USER

# download wally binary
wget https://configure.ergodox-ez.com/wally/linux -O /tmp/wally
chmod +x /tmp/wally
chown $INSTALL_USER:$INSTALL_USER /tmp/wally
mv /tmp/wally /home/$INSTALL_USER/.local/bin

# create desktop file
cat << EOF > $home/.local/share/applications/wally.desktop
[Desktop Entry]
Type=Application
TryExec=wally
Exec=wally
Terminal=false
Categories=Utility
Icon=wally

Name=Wally
Comment=ZSA configure
EOF
chown $INSTALL_USER:$INSTALL_USER $home/.local/share/applications/wally.desktop

# download icon
wget https://d33wubrfki0l68.cloudfront.net/7033dde42ac38d4f2d7da49b841d2a06ecc6fbc0/682b0/static/wally-logo-4b9418edd84190e10b0704f8962e76cb.png -O /tmp/wally.png

if [ ! -d $home/.local/share/icons ]; then
    # create directory with correct permissions
    su $INSTALL_USER -c "mkdir $home/.local/share/icons"
fi

mv /tmp/wally.png $home/.local/share/icons/wally.png
chown $INSTALL_USER:$INSTALL_USER -R $home/.local/share/icons/wally.png
