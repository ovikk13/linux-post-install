#!/usr/bin/env bash

# get snap packages to uninstall
get_snap_packages() {
    cat <<-SNAP
        ubuntu-mate-welcome
        software-boutique 
	SNAP
}

# get the apt packages to uninstall
get_apt_packages() {
    cat <<-APT
        engrampa
        onboard
        pluma
        plank
        magnus
        atril
	APT
}

# echos the passed apt packages that are installed on the system
filter_apt_packages_installed() {
    for pack in $1; do
        if dpkg --list | grep -q "$pack"; then
            echo "$pack"
        fi
    done
}

# asks the user whether the passed packages should be uninstalled
ask_user_should_uninstall() {
    ./helpers/ask-user.sh "Do you want to uninstall the following: '$*'?"
    return $?
}

# uninstalls the apt pacakges
uninstall_apt_packages() {
    echo
    echo 'Quering apt packages to uninstall'

    packs=$(get_apt_packages)
    installed_packs=$(filter_apt_packages_installed "$packs")

    if [ -z "$installed_packs" ]; then
        echo 'No apt packages to uninstall'
        return
    fi

    if ask_user_should_uninstall $installed_packs; then
        apt-get purge -y $installed_packs
        apt-get autoremove -y
    else
        echo 'Skipping apts'
    fi
}

uninstall_snap_packages() {
    echo
    echo 'Finding snaps to uninstall'

    snaps=$(get_snap_packages)

    if ask_user_should_uninstall $snaps; then
        for snap in ${snaps[@]}; do
            snap remove "$snap" --purge
        done

        if [ -d /home/$INSTALL_USER/snap/ubuntu-mate-welcome ]; then
            rm -rf /home/$INSTALL_USER/snap/ubuntu-mate-welcome
        fi
    else
        echo 'Skipping snaps'
    fi
}

main() {
    uninstall_apt_packages
    uninstall_snap_packages
}

main
