#!/usr/bin/env bash

# abort on any failure
set -e

main() {
    if command -v dunst > /dev/null 2>&1; then
        if ./helpers/ask-user.sh "dunst appears to be installed. Skip?"; then
            echo 'Skipping dunst install'
            exit
        fi
    fi

    build_dunst
    replace_mate_notification_service
}

# builds dunst
build_dunst() {
    # dependencies
    apt-get install -y checkinstall libdbus-1-dev libx11-dev libxinerama-dev libxrandr-dev libxss-dev libglib2.0-dev libpango1.0-dev libgtk-3-dev libxdg-basedir-dev libnotify-dev

    # build dunst
    cd /usr/local/src
    rm -rf dunst # remove previous install if exists

    git clone https://github.com/dunst-project/dunst.git
    cd dunst

    PREFIX=/usr make

    # install dunst
    PREFIX=/usr checkinstall
    dpkg -i dunst_*_amd64.deb
}

# replaces the mate notification service with dunst
replace_mate_notification_service() {
    # replace mate notification service with dunst
    dunst=$(command -v dunst)
    escaped=$(printf '%s\n' "$dunst" | sed -e 's/[\/&]/\\&/g') # escape / chars

    # replace Exec with path to dunst
    sed -i "s/Exec=.*/Exec=$escaped/g" /usr/share/dbus-1/services/org.freedesktop.mate.Notifications.service
}

main
