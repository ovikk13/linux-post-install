#!/usr/bin/env bash

# abort if any commands fail
set -e

if command -v nvim &> /dev/null; then
    if ./helpers/ask-user.sh "nvim appears to be installed. Do you want to skip? "; then
        echo 'Skipping neovim installation'
        exit 0;
    fi
fi

# make src directory (should be present)
mkdir -p /usr/local/src
cd /usr/local/src

# install dependencies
apt-get -y install ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config unzip

# clone
rm -rf nvim
git clone https://github.com/neovim/neovim.git nvim

# build
cd nvim
make CMAKE_BUILD_TYPE=Release
make install
