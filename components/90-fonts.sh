#!/usr/bin/env bash

set -e

install_fonts() {
    # donwload and unzip fonts
    wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/UbuntuMono.zip -O /tmp/UbuntuMono.zip
    unzip /tmp/UbuntuMono.zip -d /tmp/UbuntuMono

    mkdir -p ~/.local/share/fonts

    # this will copy all Mono fonts, but not the Windows fonts, to ~/.local/share/fonts
    ls /tmp/UbuntuMono | egrep 'Mono.ttf$' | grep -v 'Windows' | xargs -I {} cp /tmp/UbuntuMono/{} ~/.local/share/fonts

    # rebuild the cache
    fc-cache -f -v

    if ! fc-list | grep -q 'UbuntuMono Nerd'; then
        echo 'Failed to install UbuntuMono Nerd font' >&2
    fi
}

# run this as the user to make sure we get the right permissions
export -f install_fonts
su "$INSTALL_USER" -c "bash -c install_fonts"

