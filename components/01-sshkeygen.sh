#!/usr/bin/env bash

home="/home/$INSTALL_USER"

if [ -f $home/.ssh/id_rsa ]; then
    if ./helpers/ask-user.sh "There seems to be an existing ssh keypair already. Do you want to skip?"; then
        echo 'Skipping sshkeygen'
        exit
    fi
fi

read -p "Where do you want to install the keyfile? ($home/.ssh/id_rsa): " keyfile

if [ -z "$keyfile" ]; then
    keyfile="$home/.ssh/id_rsa"
fi

su $INSTALL_USER -c "ssh-keygen -f $keyfile"

public_keyfile="$keyfile.pub"

if [ ! -f "$public_keyfile" ]; then
    echo "Could not find public key file at: $public_keyfile"
    exit 1;
fi

echo "Copying content of $public_keyfile to clipboard"
cat "$public_keyfile" | xclip -selection clipboard

read -p "Please paste the contents to gitlab (needed for yadm). Press enter to continue"
