#!/usr/bin/env bash

home=/home/$INSTALL_USER

if [ ! -d $home/.cargo ]; then
    echo 'Could not find rustup. Run the rustup installer' >&2
    exit 1
fi

if [ -d /usr/local/src/alacritty ]; then
    if ./helpers/ask-user.sh "Alacritty appears to be installed. Skip?"; then
        echo 'Skipping install of alacritty'
        exit
    fi
fi

set -e

# make sure we have correct path
export HOME=$home # hack to make cargo set correct path
source $home/.cargo/env

echo $PATH

# make sure we have correct compiler
rustup override set stable
rustup update stable

# dependencies
apt-get install cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev python3 gzip

# clone source
cd /usr/local/src

rm -rf alacritty
git clone https://github.com/alacritty/alacritty.git

# build
cd alacritty
cargo build --release

# check if the terminfo is installed
if ! infocmp alacritty > /dev/null 2>&1; then
    tic -xe alacritty,alacritty-direct extra/alacritty.info
fi

# install .desktop file
cp target/release/alacritty /usr/local/bin
cp extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg
desktop-file-install extra/linux/Alacritty.desktop
update-desktop-database

# install man page
mkdir -p /usr/local/share/man/man1
gzip -c extra/alacritty.man > /usr/local/share/man/man1/alacritty.1.gz

# install shell completion for fish
if command -v fish > /dev/null 2>&1; then
    fish -i <<-EOF
        mkdir -p \$fish_complete_path[1]
        cp extra/completions/alacritty.fish \$fish_complete_path[1]/alacritty.fish
	EOF
fi
