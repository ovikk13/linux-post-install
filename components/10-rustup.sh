#!/usr/bin/env bash

if [ -d /home/$INSTALL_USER/.rustup ]; then
    if ./helpers/ask-user.sh "Rustup appears to be installed. Skip?"; then
        echo 'Skipping rustup'
        exit
    fi
fi

set -e

# use sudo here to avoid errors with /dev/tty. (su does not have a controlling terminal)
sudo -u "$INSTALL_USER" bash -c "curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh"

# include cargo in path
source "/home/$INSTALL_USER/.cargo/env"
