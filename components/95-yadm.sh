#!/usr/bin/env bash

# abort on failure
set -e

home=/home/$INSTALL_USER

main() {
    install_yadm

    if [ -d $home/.local/share/yadm/repo.git ]; then
        if ! ./helpers/ask-user.sh "dotfiles repo appears to already be cloned. Do you want to skip?"; then
            clone_dotfiles
        else
            if ./helpers/ask-user.sh "do you want to run the bootstrap script?"; then
                run_bootstrap
            fi
        fi
    else
        clone_dotfiles
    fi
}

install_yadm() {
    if [ -f /tmp/yadm ]; then
        return
    fi

    wget https://raw.githubusercontent.com/TheLocehiliosan/yadm/master/yadm -O /tmp/yadm
    chmod +x /tmp/yadm
}

clone_dotfiles() {
    DOTFILES_REPO="git@gitlab.com:ovikk13/dotfiles.git"


    su "$INSTALL_USER" <<-EOF
        set -e

        /tmp/yadm clone -f "$DOTFILES_REPO"
        /tmp/yadm reset HEAD --hard
        /tmp/yadm submodule update --init --recursive
        echo 'hi'
	EOF

    mkdir -p $home/.local/bin
    rm $home/.local/bin/yadm

    ln -s $home/.config/yadm/yadm.git/yadm $home/.local/bin/yadm

    add_dotfiles_ignore
}

run_bootstrap() {
    su "$INSTALL_USER" -c "/tmp/yadm bootstrap"
}

add_dotfiles_ignore() {
    su "$INSTALL_USER" -c "git config --file $home/.local/share/yadm/repo.git/config core.excludesFile $home/.dotfiles.ignore"
}

main

