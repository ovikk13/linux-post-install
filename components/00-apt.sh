#!/usr/bin/bash

# Abort if something fails
set -e

get_packages() {
    cat <<-EOF
        fish

        git
        tmux

        htop

        fd-find

        xclip
        xcape

        curl

        python3-pip

        taskwarrior
        timewarrior
	EOF
}

apt-get install -y $(get_packages)

# issue, see https://askubuntu.com/questions/1290262/unable-to-install-bat-error-trying-to-overwrite-usr-crates2-json-which
apt-get install -o Dpkg::Options::="--force-overwrite" -y ripgrep bat
