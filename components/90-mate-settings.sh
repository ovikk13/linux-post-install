#!/usr/bin/env bash

set -e

dconf write /org/mate/desktop/peripherals/touchpad/natural-scroll true

# map caps lock to control
gsettings set org.mate.peripherals-keyboard-xkb.kbd options "['caps\tcaps:ctrl_modifier']"

