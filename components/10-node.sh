#!/usr/bin/env bash

set -e

# run as sudo since we need interactive terminal
# set SHELL to empty to force n to not do fancy stuff to our configuration
sudo -u "$INSTALL_USER" bash -c "curl -L https://git.io/n-install | SHELL="" N_PREFIX=~/.n bash"
