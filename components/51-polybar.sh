#!/usr/bin/env bash

# abort on failure
set -e

if [ -d /usr/local/src/polybar ]; then
    if ./helpers/ask-user.sh "Polybar appears to be installed. Do you want to skip?"; then
        echo 'Skipping polybar'
        exit
    fi
fi

# dependencies
apt-get install -y build-essential git cmake cmake-data pkg-config python3-sphinx python3-packaging libcairo2-dev libxcb1-dev libxcb-util0-dev libxcb-randr0-dev libxcb-composite0-dev python3-xcbgen xcb-proto libxcb-image0-dev libxcb-ewmh-dev libxcb-icccm4-dev

# optional dependencies
apt-get install -y libjsoncpp-dev libpulse-dev

cd /usr/local/src

rm -rf polybar
git clone --recursive https://github.com/polybar/polybar.git
cd polybar

mkdir build
cd build
cmake ..
make -j$(nproc)

# install polybar to /usr/local/bin
make install
