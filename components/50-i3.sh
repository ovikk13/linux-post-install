#!/usr/bin/env bash

# abort on first failure
set -e

# install packages needed for i3 system
apt-get install i3-wm rofi dunst

su "$INSTALL_USER" <<-GSETTINGS
    gsettings set org.mate.session.required-components windowmanager i3

    # remove filemanager, dock and panel from required components
    gsettings set org.mate.session required-components-list "['windowmanager']"
	GSETTINGS

# disable mate autostart that conflicts with i3
if [ -d /usr/share/mate/autostart ]; then
    mv /usr/share/mate/autostart /usr/share/mate/autostart.disabled
fi
