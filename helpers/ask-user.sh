#!/usr/bin/env bash

while true
do
    read -p "$1 [Y/n] " yn
    case $yn in 
        [Yy]* | "") exit 0;;
        [Nn]* ) exit 1;;
        * ) echo "Please answer y/n" >&2
    esac
done
