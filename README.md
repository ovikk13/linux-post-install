Post Install for Linux
======================
This is a script to configure a linux system after an installation. This script currently only works on Ubuntu.

### Usage
Run the script by calling `post-install.sh`. 

Options:
 - `-f`: Filter components by this string. This is string is passed straight to `egrep`.
 - `-s`: Skip `apt-get update` on install. If this is not present, `post-install.sh` will run `apt-get update` prior
         to running any components.

### Design
The script is modular, consisting of multiple *components*. Each component is an install script. Components are
located in the `components` folder.

The install script can be of any type. The shebang defines the script type. 

The components are run in aplhabetical order, so the number prefix defines the order. Dependencies should therefore
have low number prefix so they are run early on.

Try have to have as few user input queries in the install components, so the script can run in the background. If a
component requires user input, have low number prefix so they are run early on.

### ./helpers
This directory contains helper scripts for components. They can be run from components using `./helpers/<script>`, this
the working directory will always be the directory `post-install.sh` is in:

 - `ask-user.sh`: Asks the user a yes/no answer (default yes). Usage: `./helpers/ask-user.sh "The question you want to ask?"`.
    Exit status 0 on yes, and 1 on no.

### Environment variables
The following environment variables are passed to components:
 - `$INSTALL_USER`: this is the user we are installing to. Components that require a home directory will use the home
        directory of this user.
