#!/usr/bin/env bash

FILTER=""
APT_UPDATE=1

main () {
    if [[ $UID != 0 ]]; then
        echo "Must be run as root or with sudo."
        exit 1;
    fi
    if [ ! -d components ]; then
        echo "Must be run in the same directory as the script"
        exit 2;
    fi

    parse_args "$@"

    export INSTALL_USER="$(get_user)"
    echo "Installing to user '$INSTALL_USER'"

    components=$(get_components_to_install)

    if [ -n "$components" ]; then
        apt_update

        echo
        echo "The following components will now be run: "
        echo "$components"

        run_components "$components"
    else
        echo "No components to run"
    fi

    echo
    echo "Finished installing!"
}

parse_args() {
    while getopts "f:s" opt; do
        case "$opt" in
            f) FILTER="$OPTARG" ;;
            s) APT_UPDATE=0 ;;
        esac
    done
}

run_components() {
    for component in $components
    do
        run_component $component
    done
}

run_component() {
    component="$1"

    echo
    echo "Running '$component' in 2 seconds."
    echo

    sleep 2

    "./$component"
    exit_code="$?"

    if [[ $exit_code != 0 ]]; then
        echo
        if ./helpers/ask-user.sh "'$component' failed with '$exit_code'. Do you want to retry?"; then
            run_component "$component"
        fi
    fi
}

get_components_to_install() {
    components=$(find_components)

    for component in $components
    do
        if ./helpers/ask-user.sh "Do you want to run '$component'"; then
            echo "$component"
        fi
    done
}

find_components() {
    components="$(find components -type f | sort)"

    if [ -n "$FILTER" ]; then
        echo $(echo "$components" | egrep "$FILTER")
    else
        echo "$components"
    fi
}

apt_update() {
    if [[ "$APT_UPDATE" == 1 ]]; then
        echo
        echo "Calling apt-get update"
        apt-get update
    fi
}

get_user() {
    if [ -n "$SUDO_USER" ]; then
        echo "$SUDO_USER"
    else
        read -p "What user will the system be using? " user
        echo "$user"
    fi
}

main "$@"
